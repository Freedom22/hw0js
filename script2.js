





//-------------------------------- 2 -------------------------------------

let select = document.createElement('select');
select.id = "rating";


const main = document.querySelector('main');
const sectionFeatures = document.getElementById('features');
main.insertBefore(select, sectionFeatures);




const ratingSelect = document.getElementById("rating");
 
let ratingOptions = [
    {value: '4', text: '4 Stars' },
    {value: '3', text: '3 Stars' },
    {value: '2', text: '2 Stars' },
    {value: '1', text: '1 Stars' },
]

ratingOptions.forEach(function(option){
    let optionElemetn = document.createElement('option');
    optionElemetn.value = option.value;
    optionElemetn.textContent = option.text;
    ratingSelect.appendChild(optionElemetn);
})